from behave.__main__ import main as run_behave
import os


api_url = os.environ.get('API_URL', 'http://localhost:5010')
api_paths = {
    'health-check': '/',
    'ping': '/ping',
    'auth-ping': '/ping/auth',
    'admin-ping': '/ping/admin',
    'login': '/auth/login',
    'logout': '/auth/logout',
    'users': '/users',
    'sketches': '/sketches'
}
status_codes = {
    'ok': 200,
    'unauthorized': 401,
    'forbidden': 403,
    'bad request': 404
}


def get_headers(context):
    if context.token:
        return {'Authorization': f'Bearer {context.token}'}
    else:
        return ''


if __name__ == '__main__':
    try:
        run_behave('src/features --junit --junit-directory reports')
    except Exception as e:
        print(e)
