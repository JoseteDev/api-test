Feature: ping feature
  # Enter feature description here


  Scenario: Check ping
    When we do ping request
    Then response is ok


  Scenario: Check auth ping
    When we do auth-ping request
    Then response is unauthorized

    When we do login request with credentials qatester@emilio.com Patata100
    Then response is ok

    When we do auth-ping request
    Then response is ok


  Scenario: Check admin ping
    When we do login request with credentials user@emilio.com Patata100
    Then response is ok

    When we do admin-ping request
    Then response is forbidden

    When we do logout request
    Then response is ok

    When we do login request with credentials qatester@emilio.com Patata100
    Then response is ok

    When we do admin-ping request
    Then response is ok
