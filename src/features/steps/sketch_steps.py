import behave
import requests
from main import api_url, api_paths, status_codes, get_headers


@when("we get sketches")
def step_impl(context):
    headers = get_headers(context)
    context.response = requests.get(url=f'{api_url}/sketches', headers=headers)


@when("create sketch {name} {longitude} {latitude}")
def step_impl(context, name, longitude, latitude):
    headers = get_headers(context)
    body = {
        'name': name,
        'longitude': longitude,
        'latitude': latitude
    }
    with open('assets/among_us_character.zip', 'rb') as sketch_model:
        response = requests.post(url=f'{api_url}/sketches', headers=headers, data=body, files={'zip_file': sketch_model})
    context.response = response
    context.sketch = response.json().get('data').get('sketch')


@when("we remove sketch {name}")
def step_impl(context, name):
    assert name == context.sketch.get('name')
    sketch_id = context.sketch.get('id')
    headers = get_headers(context)
    context.response = requests.delete(url=f'{api_url}/sketches/{sketch_id}', headers=headers)


@step("sketch {name} exists is {exists}")
def step_impl(context, name, exists):
    sketch_list = context.response.json().get('data').get('sketch_list')
    found = False
    for sketch in sketch_list:
        if sketch.get('name') == name:
            context.sketch = sketch
            found = True
    assert eval(exists) == found
