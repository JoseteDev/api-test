import behave
import requests
from main import api_url, api_paths, status_codes, get_headers


@when("we get users")
def step_impl(context):
    headers = get_headers(context)
    context.response = requests.get(url=f'{api_url}/users', headers=headers)


@when("we create user {email} {username} {password}")
def step_impl(context, email, username, password):
    body = {
        'email': email,
        'username': username,
        'password': password
    }
    response = requests.post(url=f'{api_url}/users', json=body)
    context.response = response
    context.user = response.json().get('data').get('user')


@when("we update user {email} with {username}")
def step_impl(context, email, username):
    assert email == context.user.get('email')
    user_id = context.user.get('id')
    headers = get_headers(context)
    body = {
        'username': username
    }
    context.response = requests.put(url=f'{api_url}/users/{user_id}', headers=headers, json=body)


@when("we remove user {email}")
def step_impl(context, email):
    assert email == context.user.get('email')
    user_id = context.user.get('id')
    headers = get_headers(context)
    context.response = requests.delete(url=f'{api_url}/users/{user_id}', headers=headers)


@step("user {email} exists is {exists}")
def step_impl(context, email, exists):
    user_list = context.response.json().get('data').get('user_list')
    found = False
    for user in user_list:
        if user.get('email') == email:
            context.user = user
            found = True
    assert eval(exists) == found
