import behave
import requests
from main import api_url, api_paths, status_codes, get_headers


@then('response is {status_code}')
def step_impl(context, status_code):
    assert context.response.status_code == status_codes.get(status_code)


@when('we do {request} request')
def step_impl(context, request):
    path = api_paths.get(request)
    assert path is not None
    headers = get_headers(context)
    if request == 'logout':
        context.response = requests.post(url=api_url + path, headers=headers)
    else:
        context.response = requests.get(url=api_url + path, headers=headers)


@when('we do login request with credentials {email} {password}')
def step_impl(context, email, password):
    path = api_paths.get('login')
    assert path is not None
    body = {
        'email': email,
        'password': password
    }
    response = requests.post(url=api_url + path, json=body)
    context.response = response
    if response.status_code == 200:
        context.token = response.json().get('data').get('jwt')
        assert context.token is not None
