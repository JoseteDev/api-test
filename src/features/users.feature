Feature: users feature
  # Enter feature description here


  Scenario: try to login with an unexisting user
    When we do login request with credentials qa-test-user@emilio.com qa-test-user Patata100
    Then response is unauthorized


  Scenario: create a new user and login with it
    When we create user qa-test-user@emilio.com qa-test-user Patata100
    Then response is ok

    When we do login request with credentials qa-test-user@emilio.com Patata100
    Then response is ok

    When we do logout request
    Then response is ok


  Scenario: login with qa user, edit the created user and remove it
    When we do login request with credentials qatester@emilio.com Patata100
    Then response is ok

    When we get users
    Then response is ok
    And user qa-test-user@emilio.com exists is True

    When we update user qa-test-user@emilio.com with qa-test-user-edited
    Then response is ok

    When we remove user qa-test-user@emilio.com
    Then response is ok

    When we get users
    Then response is ok
    And user qa-test-user@emilio.com exists is False