Feature: sketches feature
  # Enter feature description here


  Background:
    When we do login request with credentials qatester@emilio.com Patata100
    Then response is ok


  Scenario: CRUD a new sketch
    When create sketch qa-testing-sketch -5.215790 39.647764
    Then response is ok

    When we get sketches
    Then response is ok
    And sketch qa-testing-sketch exists is True

    When we remove sketch qa-testing-sketch
    Then response is ok

    When we get sketches
    Then response is ok
    And sketch qa-testing-sketch exists is False
