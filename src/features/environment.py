
def before_all(context):
    """
    Called before all features
    Nothing
    """
    context.token = None
    context.user = None
    context.sketch = None


def after_all(context):
    """
    Called after all features
    Nothing
    """
    pass
